(function($){	

	Aloha.ready( function() {			
		setTimeout(function(){		
			
			//Limit input length
			Aloha.bind('aloha-editable-created', function(event, $editable){
				
					var maxLength = 0;							
					if($editable.originalObj.attr('maxlength'))
					{
						maxLength = parseInt($editable.originalObj.attr('maxlength'));	
					}							 
					
					//Fix Width
					$editable.obj.css('width', '98%');
										
					//Fix Height
					$editable.obj.css('height', 'auto');			
					
					if(maxLength > 0)
					{
						$editable.obj.addClass('maxlength-limted');
						$editable.obj.after('<p class="maxlength">Characters left: <span class="charsLeft">' + maxLength + '</span></p>');						
						
						var $counter = $editable.obj.next().find('.charsLeft');
						
						var onFormatSingleLineText = function(trimIt)
						{
							//Remove breaks
							var breaks = $editable.obj.find('br');
							if(breaks.length)
							{
								breaks.after('&nbsp;').remove();
							}
							
							//Put all content in a single p tag
							var pars = $editable.obj.find('p');
							var html = '';
							if(pars.length > 1)
							{									
								pars.each(function(){
									var p = $(this);
									html += p.html();
								});																										
							}	
							else if(pars.length == 0)
							{
								html = $editable.obj.html();																									
							}
							else if(trimIt) //single
							{
								html = pars.eq(0).html();
							}
							
							if(trimIt && html.length)
							{								
								html = $.trim(html);
								$editable.setContents('<p>' + html + '</p>');
							}								
						}
						    					
						onFormatSingleLineText(true);									
						var previousContent = $editable.obj.html();
						
						var failsafe = 0;
						var timeoutEdit = 0;
						var onLengthTest = function() {		
							
							failsafe++;
							if(failsafe > 10)
							{									
								return false;
							}
							onFormatSingleLineText();
							
													
							//Strip tags
							var clearText = $.trim($editable.getContents().replace(/<\/?[^>]+>/gi, '').replace(/&nbsp;/g, '#'));
							var charsLeft = maxLength - clearText.length;								
							
							if(charsLeft < 0)
							{
								if($editable.obj.html() != previousContent)
								{						
									$editable.setContents(previousContent);										
									return onLengthTest();																	
								}
								else
								{						
									var children = $editable.obj.find('> *');	
									if(children.length)
									{
										var subtracted = false;
										for(var i = children.length - 1; i >= 0; i--)
										{
											var $element = $(children[i]);
											var $contents = $element.contents();		
											
											for(var j = $contents.length - 1; j >= 0; j--)
											{
												var node = $contents[j];													
												//Detect text
												if(node.data != undefined)
												{
													if(node.data.length)
													{															
														node.data = node.data.substr(0, Math.min(node.data.length, node.data.length + charsLeft));
														subtracted = true;
														break;
													} 
												}
												else
												{
													var $node = $(node);
													var html = $node.html();
													if(html.length)
													{
														$node.html( html.substr(0, Math.min(html.length, html.length + charsLeft)) );
														subtracted = true;
														break;
													}
												}
											}
											
											if(subtracted)
											{
												break;
											}
										}
										
										if(subtracted)
										{
											return onLengthTest();
										}
									}									
								}
							}
							
							$counter.html(charsLeft);	
							previousContent = $editable.obj.html();
							
							return true;
						}		
						onLengthTest();	
						
						$editable.obj.keydown(function(event) {
							failsafe = 0;
							clearTimeout(timeoutEdit);			
							timeoutEdit = setTimeout(onLengthTest, 10);																					
						});
						$editable.obj.keyup(function(event) {
							failsafe = 0;								
							clearTimeout(timeoutEdit);			
							timeoutEdit = setTimeout(onLengthTest, 10);													
						});
						$editable.obj.blur(function(event) {
							failsafe = 0;								
							clearTimeout(timeoutEdit);			
							timeoutEdit = setTimeout(onLengthTest, 10);								
						});			
					}						
			});		
			
																
			Drupal.behaviors.alohaeditor = function()
			{
				if(typeof(Drupal.settings.alohaeditor.elements) != 'undefined')
				{
				    var strQuery = '';
				    for(var i in Drupal.settings.alohaeditor.elements)
				    {
				        var id = Drupal.settings.alohaeditor.elements[i][0];
				        var elementQuery = '#' + id;
				        
				        //Handle multi-groups & multi-values
				        if(elementQuery.indexOf('-0-') >= 0)
				        {
				        	var newQuery = elementQuery;
				        	var selIndex = 0;
				        	var $selection = null;
				        	do {
				        		selIndex++;
				        		var selQuery = elementQuery.replace('-0-', '-' + selIndex.toString() + '-');
				        		$selection = $(selQuery);
				        		if($selection.length)
				        		{
				        			newQuery += ', ' + selQuery;
				        		}
				        	}
				        	while($selection.length && selIndex < 200); //Failsafe
				        	elementQuery = newQuery;
				        }
				         
				        if(parseInt(Drupal.settings.alohaeditor.elements[i][1]) > 0)
				        {
				        	$(elementQuery).attr('maxlength', Drupal.settings.alohaeditor.elements[i][1]);
				        }
				        
				        strQuery += elementQuery;
				        if(i < Drupal.settings.alohaeditor.elements.length - 1)
				        {
				            strQuery += ',';
				        }
				    }				        
				        
				    var $alohaeditor = $(strQuery).filter(':not(.alohaeditor-processed, .alohaeditor-disabled)');
				    $alohaeditor.addClass('alohaeditor-processed');
				    			    			   	
				   	if($alohaeditor.length)
				   	{
				   		$alohaeditor.each(function(){
				   			var $each = $(this);
				   			var html = $each.val();
						   	html = html.replace("\n", '');					   	
						   	$each.val(html);	
				   		});
					   	
					   	//Handle content ourselves, because of br bugs in aloha
					    $alohaeditor.hide();
					    $alohaeditor.each(function(){
					    	var $original = $(this);
					    	var $clone = $original.clone();
					    	$clone.attr('id', $clone.attr('id') + '-temp').removeAttr('name');
					    	$original.hide().after($clone);
					    	
					    	//Fix for multiple CCK values
							var $tr = $original.parents('tr.draggable:first');
							if(!$tr.length)
							{
								$tr = $original.parents('.fieldset-content:first');
							}
							
							if($tr.length)
							{
								$tr.find('ul.tips').hide().next().hide();
								$tr.find('.form-item').find('.content-multiple-remove-button, .form-item-filter:last select').hide();
								var $delta = $tr.find('.form-item-filter select')
								if($delta.length)
								{
									var strClass = $delta.attr('id');							
									var indClass = strClass.indexOf('-0-');
									if(indClass > 0)
									{
										strClass = strClass.substr(5, indClass-5).replace(/\-/g, '_') + '-delta-order';
										$delta.addClass(strClass);
									}								
								}
								$tr.find('.content-multiple-remove-cell .content-multiple-remove-button').show();
								$tr.find('td.content-multiple-drag').next().css('width', '98%');
								
								//Rewire CCK multi-field events
								var intervalRewire = setInterval(function(){
									var $table = $original.parents('table.content-multiple-table');
									if(!$table.length)
									{
										clearInterval(intervalRewire);
										return;
									}
									
									var $submit = $table.next().find('.form-submit');
									//$ is not the same jquery as in Drupal Core!
									var events = jQuery.data($submit[0], "events");									
									if(events)
									{										
										clearInterval(intervalRewire);
										
										var functionsMouseDown = new Array();
										for (var name in events.mousedown) 
										{
											functionsMouseDown.push(events.mousedown[name]);								   
										}
										
										var functionsKeypress = new Array();
										for (var name in events.keypress) 
										{
											functionsKeypress.push(events.keypress[name]);								   
										}
										
										//Process add more values
										$submit = jQuery($submit[0]);								
										$submit.unbind('mousedown keypress').mousedown(function(){									
											onAlohaSave();
										});
										
										for (var name in functionsMouseDown) 
										{
										   $submit.mousedown(functionsMouseDown[name]);	
										}										
										for (var name in functionsKeypress) 
										{
										   $submit.keypress(functionsKeypress[name]);	
										}
									}	
								}, 100); 
									
							}							
					    	
					    	$clone.aloha();
					    });				    
				    }			    
				}
			}	
			Drupal.behaviors.alohaeditor();	
			
			
			Aloha.bind('aloha-editable-activated', function() {
				//Fix empty button groups
				$('.aloha-ui-component-group').each(function(){
					var group = $(this);
					if(!group.find('button').length)
					{
						group.remove();
					}
				})
			});		
			
			//For avoiding accidental pressing of back button
			$(document).keydown(function(event){
				if(event.metaKey && !event.shiftKey) //cmd (osx)
			    {
			    	if(event.keyCode == 37 || event.keyCode == 39) //left / right
			    	{
			    		event.preventDefault();		
			    	}
			    }		  
			});
			
			//Avoid new lines in maxlength fields
			$(window).bind("keyup keydown keypress", function(event){			
				if(event.keyCode == 13)
				{
					var $editable = Aloha.getActiveEditable();
				    if($editable)
				    {
				    	if($editable.obj.next().is('.maxlength'))
				    	{
				    		event.preventDefault();
							event.stopPropagation();		
				    	}
			    	}
			    }		
			})
			
			function onAlohaSave()
			{
				for(var i in Aloha.editables)
				{
					var editable = Aloha.editables[i];
					var val = editable.obj.text(); 
					if(val.length)
					{
						val = editable.obj.html()
					}					
					editable.originalObj.prev().val(val);
				}
			}
			
			//Link saving of contents
			$('form#node-form').submit(function(){
				onAlohaSave();
			});
			
			Aloha.saveAll = onAlohaSave;
			
		}, 10); //Timeout
		
	});
	
})(Aloha.settings.jQuery);
