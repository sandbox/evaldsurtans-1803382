<?php

define('ALOHAEDITOR_PLUGINS', serialize(array(
		'common/ui',
		'common/commands',
		'common/contenthandler',
		'common/format',
		'common/list',
		'common/undo',
		'common/link',
		'common/paste',
		'extra/formatlesspaste' 		
		//,'common/table' 
	)) );
	
