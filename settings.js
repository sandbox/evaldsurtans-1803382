Aloha = window.Aloha || {};
Aloha.settings = window.Aloha.settings || {};
Aloha.settings.jQuery = jQuery_1_7_2;

(function($){	
	var settings = {
		contentHandler: {
		    insertHtml: [ 'generic', 'sanitize' ],
		    initEditable: [ 'generic', 'sanitize' ],
		    sanitize: 'relaxed', // relaxed, restricted, basic,
		    allows: {
		        elements: [
		            'strong', 'em', 'i', 'b', 'blockquote', 'br', 'cite', 'dd', 'div', 'dl', 'dt', 'em',
		            'i', 'li', 'ol', 'pre', 'q', 'small', 'strike', 'sub', 'p', 'a',
		            'sup', 'u', 'ul'],
		 
		        attributes: {
		            'a'         : ['href']	         
		         }
		    }
		},
		toolbar : {		
			tabs: [			
					{
						label: 'Format',				
						components: [
							[						
								'bold', 'italic'					
							],
							['formatLink'],
							['orderedList', 'unorderedList'],							
						]
					}
				],			
			exclude: ['indentList', 'outdentList', 
								'subscript', 'superscript', 'strikethrough', 'quote', 'formatBlock',
								'formatAbbr', 'formatNumeratedHeaders', 
								'toggleMetaView', 'wailang',  'toggleFormatlessPaste', 
								'toggleDragDrop', 'alignLeft', 'alignCenter', 'alignRight', 'alignJustify',
								 'indentList', 'outdentList', 'formatBlock', 'tableCaption', 'tableSummary', 'formatTable']
		},
		plugins : {
			link: {
				targetregex: '^(?!.*latvia.lv).*',
				objectTypeFilter: [],
			},
			format : {
				config: ['b', 'i', 'u']
			},
			formatlesspaste: {
		        config: {
		            button: true,
		            formatlessPasteOption: true, 
		            strippedElements: [ 
		                "em",
		                "strong",
		                "small",
		                "s",
		                "cite",
		                "span",
		                'h1',
		                'h2',
		                'h3',
		                'h4',
		                'h5',
		                "q",
		                "img",
		                "div",
		                "ul",
		                "ol",
		                "li",
		                "table",
		                "tr",
		                "td",
		                "th",
		                "dfn",
		                "abbr",
		                "time",
		                "code",
		                "var",
		                "samp",
		                "kbd",
		                "sub",
		                "sup",
		                "i",
		                "b",
		                "u",
		                "mark",
		                "ruby",
		                "rt",
		                "rp",
		                "bdi",
		                "bdo",
		                "ins",
		                "del"
		            ]
		        }
		    }
		},
		sidebar: {
	        disabled: true
		},
		repositories: {
			linklist: {
				data: [					
				]
			},
			timeout: 9000 
		}		
	};
	
	Aloha.settings = $.extend({}, Aloha.settings, settings);
})(jQuery_1_7_2);